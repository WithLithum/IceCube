﻿namespace IceCube.IO.Unix;

using JetBrains.Annotations;
using System;

/// <summary>
/// Represents a single digit in the octat digit format.
/// </summary>
[PublicAPI]
[Flags]
public enum UnixAccessDigit
{
    /// <summary>
    /// No permission.
    /// </summary>
    None = 0,
    /// <summary>
    /// Can execute this file as an executable program, or as a script. May also search this file.
    /// </summary>
    Execute = 1,
    /// <summary>
    /// Can write to this file.
    /// </summary>
    Write = 2,
    /// <summary>
    /// Can read from this file.
    /// </summary>
    Read = 4
}