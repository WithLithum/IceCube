﻿namespace NexusKrop.IceCube.IO;

/// <summary>
/// An enumeration of file access modes.
/// </summary>
public enum FileAccessMode
{
    /// <summary>
    /// Read access.
    /// </summary>
    Read,
    /// <summary>
    /// Write access.
    /// </summary>
    Write,
    /// <summary>
    /// Execution access.
    /// </summary>
    Execute
}
