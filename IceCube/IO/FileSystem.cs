﻿namespace NexusKrop.IceCube.IO;

using NexusKrop.IceCube.Exceptions;
using NexusKrop.IceCube.Interop;
using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.InteropServices;

#if NET6_0_OR_GREATER
using System.Runtime.Versioning;
#endif

/// <summary>
/// Provides advanced file system services.
/// </summary>
public static partial class FileSystem
{
    #region Public Methods
    /// <summary>
    /// Determines whether the specified file is a valid executable.
    /// </summary>
    /// <param name="file">The file to check.</param>
    /// <returns><see langword="true"/> if the executable is a valid executable; otherwise, <see langword="false"/>.</returns>
    public static bool IsExecutable(string file)
    {
        Checks.FileExists(file);

#if NET6_0_OR_GREATER
        if (!Path.IsPathFullyQualified(file))
        {
            file = Path.GetFullPath(file);
        }
#else
        file = Path.GetFullPath(file);
#endif

#if NET6_0_OR_GREATER
        if (OperatingSystem.IsWindows())
#else
        if (Environment.OSVersion.Platform == PlatformID.Win32NT)
#endif
        {
            return InternalIsExecutableWindows(file);
        }

#if NET7_0_OR_GREATER
        return InternalIsExecutableLinux(file);
#else
        throw new PlatformNotSupportedException();
#endif
    }

    #endregion

    #region Internal Implementations
    /// <summary>
    /// Provides Windows implementation for <see cref="IsExecutable(string)"/>.
    /// </summary>
    /// <param name="file">The file to check.</param>
    /// <returns><see langword="true"/> if the executable is a valid Win32 executable.</returns>
    /// <exception cref="Win32Exception"></exception>
#if NET6_0_OR_GREATER
    [SupportedOSPlatform("windows")]
#endif
    private static bool InternalIsExecutableWindows(string file)
    {
        if (!WinKernel.GetBinaryTypeW(file, out var dwType))
        {
            var errno = Marshal.GetLastWin32Error();

            if (errno == 0x000000C1)
            {
                return false;
            }

            throw new Win32Exception(errno);
        }

        // See https://learn.microsoft.com/en-us/windows/win32/api/winbase/nf-winbase-getbinarytypew#parameters
        return dwType == 0 || dwType == 6;
    }

#if NET7_0_OR_GREATER
    [UnsupportedOSPlatform("windows")]
    private static bool InternalIsExecutableLinux(string file)
    {
        var fileMode = File.GetUnixFileMode(file);

        if (!fileMode.HasFlag(UnixFileMode.UserExecute)
            && !fileMode.HasFlag(UnixFileMode.OtherExecute)
            && !fileMode.HasFlag(UnixFileMode.GroupExecute))
        {
            return false;
        }

        using (var binStream = new BinaryReader(File.OpenRead(file)))
        {
            if (binStream.ReadByte() != 0x7F
                || binStream.ReadChar() != 'E'
                || binStream.ReadChar() != 'L'
                || binStream.ReadChar() != 'F')
            {
                return false;
            }
        }

        return true;
    }
#endif


    #endregion
}
