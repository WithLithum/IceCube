﻿namespace NexusKrop.IceCube.IO;

using NexusKrop.IceCube.Interop;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Runtime.Versioning;

#if NET6_0_OR_GREATER
public static partial class FileSystem
{
    [SupportedOSPlatform("linux")]
    private static bool InternalTestAccessLinux(string fullPath, int mode)
    {
        var retVal = LibC.access(fullPath, mode);

        if (retVal == 0)
        {
            return true;
        }

        // If retVal is not zero...

        var errno = Marshal.GetLastPInvokeError();

        return errno switch
        {
            LibC.EACCES when mode != LibC.F_OK => false,
            LibC.ENOENT when mode == LibC.F_OK => false,
            _ => throw new Win32Exception(errno),
        };
    }
}
#endif
