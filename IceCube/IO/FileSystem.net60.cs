﻿namespace NexusKrop.IceCube.IO;

using NexusKrop.IceCube.Exceptions;
using NexusKrop.IceCube.Interop;
using NexusKrop.IceCube.IO;
using System;
using System.IO;
using System.Runtime.Versioning;

#if NET6_0_OR_GREATER
public static partial class FileSystem
{
    /// <summary>
    /// Determines whether the current user has the specified access the specified file.
    /// </summary>
    /// <param name="file">The file to test.</param>
    /// <param name="accessMode">The access mode to test.</param>
    /// <returns><see langword="true"/> if the user have the specified access; otherwise <see langword="false"/>.</returns>
    /// <exception cref="ArgumentException">The access mode is not valid.</exception>
    /// <exception cref="PlatformNotSupportedException">Calling on operating systems outside GNU/Linux.</exception>
    [SupportedOSPlatform("linux")]
    public static bool TestAccess(string file, FileAccessMode accessMode)
    {
        Checks.FileExists(file);
        if (!Path.IsPathFullyQualified(file))
        {
            file = Path.GetFullPath(file);
        }

        if (OperatingSystem.IsLinux())
        {
            InternalTestAccessLinux(file, accessMode switch
            {
                FileAccessMode.Read => LibC.R_OK,
                FileAccessMode.Write => LibC.W_OK,
                FileAccessMode.Execute => LibC.X_OK,
                _ => throw new ArgumentException(null, nameof(accessMode))
            });
        }

        throw new PlatformNotSupportedException();
    }
}
#endif
