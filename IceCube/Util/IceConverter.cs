﻿// Copyright (C) WithLithum & contributors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#if NET7_0_OR_GREATER
namespace IceCube.Util;

using IceCube.IO.Unix;
using JetBrains.Annotations;
using NexusKrop.IceCube.Exceptions;
using System;
using System.IO;
using System.Linq;

/// <summary>
/// Provides various parsing and conversion services.
/// </summary>
public static class IceConverter
{
    /// <summary>
    /// Converts a digit character to a numerical value representing a single digit (which is such digit). 
    /// </summary>
    /// <param name="digit">The digit to convert from.</param>
    /// <returns>The converted digit.</returns>
    [PublicAPI]
    public static byte ToDigitUInt8(char digit)
    {
        return digit switch
        {
            '0' => 0,
            '1' => 1,
            '2' => 2,
            '3' => 3,
            '4' => 4,
            '5' => 5,
            '6' => 6,
            '7' => 7,
            '8' => 8,
            '9' => 9,
            _ => throw new ArgumentException(ExceptionHelperResources.ConvertDigitNotDigit)
        };
    }

    /// <summary>
    /// Attempts to resolve the specified 3-letter token to the respective access digit format.
    /// </summary>
    /// <param name="token">The token to parse.</param>
    /// <param name="result">The result.</param>
    /// <returns><see langword="true"/> if this method is successful; otherwise, <see langword="false"/>.</returns>
    public static bool TryTokenToUnixAccessDigit(string token, out UnixAccessDigit result)
    {
        result = UnixAccessDigit.None;

        if (token.Length != 3 || token.Any(x => x != 'r'
                                                && x != 'w' 
                                                && x != 'x'
                                                && x != '-'))
        {
            return false;
        }
        
        var read = token[0] == 'r';
        var write = token[1] == 'w';
        var execute = token[2] == 'x';

        if (read)
        {
            result |= UnixAccessDigit.Read;
        }

        if (write)
        {
            result |= UnixAccessDigit.Write;
        }

        if (execute)
        {
            result |= UnixAccessDigit.Execute;
        }

        return true;
    }

    /// <summary>
    /// Assembles unix access digits for owner, group and others to <see cref="UnixFileMode"/>.
    /// </summary>
    /// <param name="owner">The owner permissions digit.</param>
    /// <param name="group">The group permissions digit.</param>
    /// <param name="others">The other users permissions digit.</param>
    /// <returns>The file mode.</returns>
    [PublicAPI]
    public static UnixFileMode AssembleDigits(UnixAccessDigit owner, UnixAccessDigit group, UnixAccessDigit others)
    {
        var result = UnixFileMode.None;

        // User
        if (owner.HasFlag(UnixAccessDigit.Read))
        {
            result |= UnixFileMode.UserRead;
        }

        if (owner.HasFlag(UnixAccessDigit.Write))
        {
            result |= UnixFileMode.UserWrite;
        }

        if (owner.HasFlag(UnixAccessDigit.Execute))
        {
            result |= UnixFileMode.UserExecute;
        }
        
        // Group
        if (group.HasFlag(UnixAccessDigit.Read))
        {
            result |= UnixFileMode.GroupRead;
        }

        if (group.HasFlag(UnixAccessDigit.Write))
        {
            result |= UnixFileMode.GroupWrite;
        }

        if (group.HasFlag(UnixAccessDigit.Execute))
        {
            result |= UnixFileMode.GroupExecute;
        }
        
        // Others
        if (others.HasFlag(UnixAccessDigit.Read))
        {
            result |= UnixFileMode.OtherRead;
        }

        if (others.HasFlag(UnixAccessDigit.Write))
        {
            result |= UnixFileMode.OtherWrite;
        }

        if (others.HasFlag(UnixAccessDigit.Execute))
        {
            result |= UnixFileMode.OtherExecute;
        }
        
        // Result
        return result;
    }

    /// <summary>
    /// Attempts to parse the specified access mode token.
    /// </summary>
    /// <param name="token">The token.</param>
    /// <param name="accessMode">The resulting access mode representation.</param>
    /// <returns><see langword="true"/> if this method is successful; otherwise, <see langword="false"/>.</returns>
    [PublicAPI]
    public static bool TryParseAccessMode(string token, out UnixFileMode accessMode)
    {
        accessMode = UnixFileMode.None;

        if (token.Length != 10)
        {
            return false;
        }

        var fileType = token[0];
        if (!char.IsAsciiLetterLower(fileType) && fileType != '-')
        {
            // FAIL: file type is invalid.
            return false;
        }

        var userToken = token[1..4];
        var groupToken = token[4..7];
        var otherToken = token[7..];

        if (!TryTokenToUnixAccessDigit(userToken, out var userMode))
        {
            return false;
        }

        if (!TryTokenToUnixAccessDigit(groupToken, out var groupMode))
        {
            return false;
        }

        if (!TryTokenToUnixAccessDigit(otherToken, out var otherMode))
        {
            return false;
        }

        accessMode = AssembleDigits(userMode, groupMode, otherMode);
        return true;
    }
}
#endif