﻿namespace NexusKrop.IceCube.Interop;
using System.Runtime.InteropServices;
#if NET6_0_OR_GREATER
using System.Runtime.Versioning;

[SupportedOSPlatform("windows")]
#endif
#if NET7_0_OR_GREATER
internal static partial class WinKernel
#else
internal static class WinKernel
#endif
{
#if NET7_0_OR_GREATER
    [LibraryImport("kernel32.dll", StringMarshalling = StringMarshalling.Utf16, SetLastError = true)]
    [return: MarshalAs(UnmanagedType.Bool)]
    internal static partial bool GetBinaryTypeW(string lpApplicationName, out uint dwType);
#else
    [DllImport("kernel32.dll", CharSet = CharSet.Unicode, SetLastError = true)]
    internal static extern bool GetBinaryTypeW(string lpApplicationName, out uint dwType);
#endif
}
