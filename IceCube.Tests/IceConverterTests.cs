﻿namespace NexusKrop.IceCube.Tests;

using global::IceCube.Util;

public class IceConverterTests
{
    [Test]
    public void TryParseAccessMode_BasicFunction()
    {
        Assert.That(IceConverter.TryParseAccessMode("drwxr-x---", out var accessMode));
        Assert.That(accessMode, Is.EqualTo(UnixFileMode.UserRead | UnixFileMode.UserWrite | UnixFileMode.UserExecute |
                                           UnixFileMode.GroupRead | UnixFileMode.GroupExecute));
    }
}