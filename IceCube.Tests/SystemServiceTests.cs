﻿namespace NexusKrop.IceCube.Tests;

using NexusKrop.IceCube.IO;
using System;

public class FileSystemServiceTests
{
    [Test]
    [Platform(Include = "Win32")]
    public void IsExecutable_Windows()
    {
        Assert.That(FileSystem.IsExecutable(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Windows), "system32\\cmd.exe")), Is.True);
    }

    [Test]
    [Platform(Include = "Win32")]
    public void IsExecutable_WindowsDll()
    {
        Assert.That(FileSystem.IsExecutable(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Windows), "system32\\kernel32.dll")), Is.False);
    }

    [Test]
    [Platform(Include = "Linux")]
    public void IsExecutable_Linux()
    {
        Assert.That(FileSystem.IsExecutable("usr/bin/bash"), Is.False);
    }
}
